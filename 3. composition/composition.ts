import { pipe } from "./pipe";
import {
  appendDomain,
  replaceSpacesWithHyphens,
  toLowerCase,
} from "./string-functions";
import { add1, double } from "./number-functions";
import { map, filter } from "./list-operations";

/**
 * 1. Implement the pipe function in pipe.ts to compose the string functions in string-functions.ts
 */

// The pipe function should work in the following way
// console.log(pipe("Joe Bloggs", toLowerCase, replaceSpacesWithHyphens)); // 'joe-bloggs'

/**
 * 2. Refactor the pipe function so that it can operate on generic types.
 */

// Test that it works on the number functions in number-functions.ts
// console.log(pipe(5, add1, double)); // 12

/**
 * 3. Implement the map and filter functions in list-operations.ts such that they
 * can be piped. You can implement simplified versions that don't need to take
 * index and array parameters. You should implement them in a single line using array.reduce
 */

// they should work as follows
// console.log(
//   pipe(
//     [1, 2, 3],
//     map((x) => x + 1),
//     filter((x) => x % 2 === 0)
//   )
// ); // [2,4]

/**
 * 4. Refactor the pipe function so that we can compose functions
 * independently of the initial input
 */

// it should work as follows

// const toLocalPart = pipe(toLowerCase, replaceSpacesWithHyphens);
// console.log(toLocalPart("Joe Bloggs")); // 'joe-bloggs'

/**
 * 5. Define a function called "appendDomain" in the string-functions.ts file
 * such that we can use to prepend a string with a domain
 */

// it should work as follows
// const toLocalPart = pipe(toLowerCase, replaceSpacesWithHyphens);
// const toGmail = pipe(toLocalPart, appendDomain("@gmail.com"));
// console.log(toGmail("Joe Bloggs")); // 'joe-bloggs@gmail.com'
