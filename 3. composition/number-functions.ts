// number functions
export function add1(x: number) {
  return x + 1;
}

export function double(x: number) {
  return x * 2;
}
