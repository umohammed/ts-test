export function toLowerCase(x: string): string {
  return x.toLowerCase();
}

export function replaceSpacesWithHyphens(x: string): string {
  return x.replace(/\s/g, "-");
}

// TODO
export function appendDomain() {}
