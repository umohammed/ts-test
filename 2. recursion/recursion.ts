/**
 * 1. Write a recursive function called factorial to compute the factorial of a number
 */

// TODO
function factorial() {}

// console.log(factorial(5)); // 120

/**
 * 2. Write a recursive function called sum to compute the sum of an array of numbers
 */

// TODO
function sum() {}

// console.log(sum([1, 5, 9])); // 15
// console.log(sum([])); // 0

/**
 * 3. Write a recursive function called product which takes an indefinite number of parameters
 *  and returns the product
 */

// TODO
function prod() {}

// console.log(prod(3, 4, 5, 6)); // 360
// console.log(prod()); // 1

/**
 * 4. Write a recursive function called zip which takes two arrays and zips them.
 * If the arrays are of unequal length "undefined" should be put in place of the value.
 * You can assume that the second array have a length less than or equal to the length of the first array
 */

// TODO
function zip() {}

// console.log(zip([1, 2, 3], ["a", "b", "c"])); // [[1, 'a'], [2, 'b'], [3, 'c']]
// console.log(zip([1, 2, 3], ["a"])); // [[1, 'a'], [2, undefined], [3, undefined]]
// console.log(zip([], [])); // []

/**
 * 5. Write a recursize function called filter which takes an array and a predicate
 *  and returns the filtered array
 */

// TODO: filter
function filter() {}

// console.log(filter([1,2,3], x => x % 2 === 0)) // [2]
// console.log(filter([], () => true)); // []
