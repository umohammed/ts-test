/**
 * 1. Write a function called createAdder which takes an number x as input
 * and returns a function that takes another number y as input. Calling the returned function
 * should return the sum of x and y
 */

// TODO
function createAdder() {}

// It should work the following way
// const add3 = createAdder(3);
// console.log(add3(4)); // 7

/**
 * 2. Write a function called partial which takes a function fn and an array of arguments
 * and returns fn with the arguments partially applied
 */

function createEmail(
  domain: string,
  firstName: string,
  surname: string
): string {
  return `${firstName}.${surname}@${domain}`;
}

// TODO
function partial() {}

// It should work the following way

// const createGmail = partial(createEmail, ["gmail.com"]);
// console.log(createGmail("joe", "bloggs")); // joe.bloggs@gmail.com

// const createGmailJoe = partial(createEmail, ["gmail.com", "joe"]);
// console.log(createGmailJoe("smith")); // joe.smith@gmail.com
